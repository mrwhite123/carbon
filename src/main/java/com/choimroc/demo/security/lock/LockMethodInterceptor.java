package com.choimroc.demo.security.lock;


import com.choimroc.demo.common.exception.CustomException;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Method;

/**
 * 防重复提交锁
 *
 * @author choimroc
 * @since 2019/5/1
 */
@Aspect
@Configuration
public class LockMethodInterceptor {

    @Autowired
    public LockMethodInterceptor(RedisLockHelper redisLockHelper, CacheKeyGenerator cacheKeyGenerator) {
        this.redisLockHelper = redisLockHelper;
        this.cacheKeyGenerator = cacheKeyGenerator;
    }

    private final RedisLockHelper redisLockHelper;
    private final CacheKeyGenerator cacheKeyGenerator;


    @Around("execution(public * *(..)) && @annotation(com.choimroc.demo.security.lock.CacheLock)")
    public Object interceptor(ProceedingJoinPoint pjp) {
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();
        CacheLock lock = method.getAnnotation(CacheLock.class);
        if (ObjectUtils.isEmpty(lock.prefix())) {
            throw CustomException.systemError("lock key can't be null...");
        }
        //获取锁的键名
        final String lockKey = cacheKeyGenerator.getLockKey(pjp);
        //上锁
        final Boolean success = redisLockHelper.lock(lockKey, lock.expire(), lock.timeUnit());
        //如果上锁失败，则说明锁已存在，应终止操作并返回提示
        if (Boolean.FALSE.equals(success)) {
            throw new CustomException(lock.hint());
        }
        try {
            return pjp.proceed();
        } catch (Throwable throwable) {
            throw CustomException.systemError("缓存锁异常");
        }
    }
}

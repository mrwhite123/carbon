package com.choimroc.demo.security.lock;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * @author choimroc
 * @since 2019/5/1
 */
@Configuration
@AutoConfigureAfter(RedisAutoConfiguration.class)
public class RedisLockHelper {

    private final StringRedisTemplate stringRedisTemplate;

    public RedisLockHelper(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    /**
     * 获取锁
     *
     * @param lockKey lockKey
     * @param timeout 超时时间
     * @param unit    过期单位
     * @return true or false
     */
    public Boolean lock(String lockKey, long timeout, final TimeUnit unit) {
        //如果锁存在则不操作，并返回false；如果锁不存在则上锁，并返回true
        return stringRedisTemplate.opsForValue().setIfAbsent(lockKey, "", timeout, unit);
    }

    /**
     * 解锁,在有需要时可进行解锁
     *
     * @param lockKey lockKey
     */
    public void unlock(String lockKey) {
        stringRedisTemplate.delete(lockKey);
    }

}

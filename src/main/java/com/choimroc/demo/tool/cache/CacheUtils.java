package com.choimroc.demo.tool.cache;


import com.choimroc.demo.common.Constants.RedisPrefix;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Redis缓存
 *
 * @author choimroc
 * @since 2019 /10/10
 */
@Component
public class CacheUtils extends RedisUtils {

    @Autowired
    public CacheUtils(StringRedisTemplate stringRedisTemplate) {
        super(stringRedisTemplate);
    }

    /**
     * 存储用户token,有效期为7天
     *
     * @param userId the user id
     * @param token  the userInfo
     * @return the token
     */
    public boolean setToken(Long userId, String token) {
        return set(RedisPrefix.TOKEN + userId, token, 7, TimeUnit.DAYS);
    }

    /**
     * 获取用户token
     *
     * @param userId the user id
     * @return the token
     */
    public String getToken(Long userId) {
        return get(RedisPrefix.TOKEN + userId);
    }

    /**
     * 刷新token过期时间
     *
     * @param userId the user id
     * @return the boolean
     */
    public boolean refreshToken(Long userId) {
        return expire(RedisPrefix.TOKEN + userId, 7, TimeUnit.DAYS);
    }
}

package com.choimroc.demo.tool;

import com.choimroc.demo.common.exception.CustomException;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * The type Date time utils.
 *
 * @author choimroc
 * @since 2019 /11/22
 */
public class DateTimeUtils {
    private static final String DEFAULT_DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";

    /**
     * Millis time long.
     *
     * @return the long
     */
    public static long millisTime() {
        return Clock.systemDefaultZone().millis();
    }

    /**
     * Second time long.
     *
     * @return the long
     */
    public static long secondTime() {
        return Clock.systemDefaultZone().millis() / 1000;
    }


    /**
     * Default date time string.
     *
     * @return the string
     */
    public static String nowDateTime() {
        return formatDateTime(LocalDateTime.now());
    }


    /**
     * Default date string.
     *
     * @return the string
     */
    public static String nowDate() {
        return formatDate(LocalDate.now());
    }

    /**
     * Format date time string.
     *
     * @param localDateTime the local date time
     * @param format        the format
     * @return the string
     */
    public static String formatDateTime(LocalDateTime localDateTime, String format) {
        return localDateTime.format(DateTimeFormatter.ofPattern(format));
    }

    /**
     * Format date time to yyyy-MM-dd HH:mm:ss.
     *
     * @param localDateTime the local date time
     * @return the string
     */
    public static String formatDateTime(LocalDateTime localDateTime) {
        return formatDateTime(localDateTime, DEFAULT_DATETIME_PATTERN);
    }

    /**
     * Parse date time local date time.
     *
     * @param datetime the datetime
     * @param pattern   the pattern
     * @return the local date time
     */
    public static LocalDateTime parseDateTime(String datetime, String pattern) {
        return LocalDateTime.parse(datetime, DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * parse date time yyyy-MM-dd HH:mm:ss.
     *
     * @param datetime the datetime
     * @return the local date time
     */
    public static LocalDateTime parseDateTime(String datetime) {
        return parseDateTime(datetime, DEFAULT_DATETIME_PATTERN);
    }


    /**
     * Format date string.
     *
     * @param localDate the local date
     * @param pattern    the pattern
     * @return the string
     */
    public static String formatDate(LocalDate localDate, String pattern) {
        return localDate.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * Format date time to yyyy-MM-dd.
     *
     * @param localDate the local date
     * @return the string
     */
    public static String formatDate(LocalDate localDate) {
        return formatDate(localDate, DEFAULT_DATE_PATTERN);
    }

    /**
     * Parse date local date time.
     *
     * @param date   the date
     * @param pattern the pattern
     * @return the local date time
     */
    public static LocalDate parseDate(String date, String pattern) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * parse date time yyyy-MM-dd.
     *
     * @param date the datetime
     * @return the local date time
     */
    public static LocalDate parseDate(String date) {
        return parseDate(date, DEFAULT_DATE_PATTERN);
    }

    public static void checkDateTime(String datetime, String pattern) {
        try {
            parseDateTime(datetime, pattern);
        } catch (Exception e) {
            throw new CustomException(String.format("%s 格式错误;正确格式为 %s", datetime, pattern));
        }
    }

    public static void checkDateTime(String datetime) {
        checkDateTime(datetime, DEFAULT_DATETIME_PATTERN);
    }


    public static void checkDate(String date, String pattern) {
        try {
            parseDate(date, pattern);
        } catch (Exception e) {
            throw new CustomException(String.format("%s 格式错误;正确格式为 %s", date, pattern));
        }
    }

    public static void checkDate(String date) {
        checkDate(date, DEFAULT_DATE_PATTERN);
    }
}

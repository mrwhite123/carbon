package com.choimroc.demo.tool.sn;

import com.choimroc.demo.tool.ObjCheckUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 编号生成工具
 *
 * @author choimroc
 * @since 2019/5/2
 */
public enum SnUtils {
    /**
     * 单例
     */
    INSTANCE;

    private final static Map<String, SnowFlakeVariable> VARIABLE_MAP = new HashMap<>();

    public long generate(String key) {
        SnowFlakeVariable now = VARIABLE_MAP.get(key);
        if (ObjCheckUtils.isNull(now)) {
            now = new SnowFlakeVariable(0L, 0L);
        }
        SnowFlakeVariable snowFlakeVariable = SnowFlake.INSTANCE.next(now);
        VARIABLE_MAP.put(key, snowFlakeVariable);
        return SnowFlake.INSTANCE.generateId(snowFlakeVariable);
    }

    public String generateByPrefix(String key) {
        return key + generate(key);
    }

    public String generateKey1() {
        synchronized ("key1") {
            return generateByPrefix("key1");
        }
    }

    public String generateKey2() {
        synchronized ("key2") {
            return generateByPrefix("key2");
        }
    }
}

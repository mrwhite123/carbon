package com.choimroc.demo.tool.sn;

/**
 * @author choimroc
 * @since 2020/1/14
 */
public class SnowFlakeVariable {
    /**
     * 上一次生成id的时间戳
     */
    private long lastTimestamp;
    /**
     * 毫秒内序列(0~4095)
     */
    private long sequence;

    public SnowFlakeVariable(long lastTimestamp, long sequence) {
        this.lastTimestamp = lastTimestamp;
        this.sequence = sequence;
    }

    public long getLastTimestamp() {
        return lastTimestamp;
    }

    public void setLastTimestamp(long lastTimestamp) {
        this.lastTimestamp = lastTimestamp;
    }

    public long getSequence() {
        return sequence;
    }

    public void setSequence(long sequence) {
        this.sequence = sequence;
    }
}

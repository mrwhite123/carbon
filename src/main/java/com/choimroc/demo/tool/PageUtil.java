package com.choimroc.demo.tool;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.ArrayList;
import java.util.List;

/**
 * @author choimroc
 * @since 2020/4/30
 */
public class PageUtil {

    @SuppressWarnings("unchecked")
    public static <T> IPage<T> handle(long pageNumber, long pageSize, List<List<?>> objects) {
        List<T> list;
        long total;
        if (ObjCheckUtils.isEmpty(objects)) {
            total = 0;
            list = new ArrayList<>();
        } else {
            total = (Long) objects.get(1).get(0);
            list = (List<T>) objects.get(0);
        }

        return new Page<T>().setCurrent(pageNumber).setTotal(total)
                .setSize(pageSize)
                .setRecords(list);
    }
}

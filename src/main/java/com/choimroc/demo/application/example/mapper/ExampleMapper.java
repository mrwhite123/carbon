package com.choimroc.demo.application.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.choimroc.demo.application.example.entity.Example;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author choimroc
 * @since 2019/10/17
 */
@Repository
public interface ExampleMapper extends BaseMapper<Example> {

    List<List<?>> selectByPage(
            @Param("limit") Long limit,
            @Param("offset") Long offset,
            @Param("startDate") String startDate,
            @Param("endDate") String endDate
    );

    /**
     * 优化分页
     */
    List<List<?>> optimizationPage(
            @Param("limit") Long limit,
            @Param("offset") Long offset,
            @Param("startDate") String startDate,
            @Param("endDate") String endDate

    );

    int insertReturnKey(Example example);

    int updateExample(Example example);

    List<Example> selectByIds(
            @Param("name") String name,
            @Param("ids") Long[] ids
    );

    /**
     * 用户为多角色，使用,分割
     * 如role_ids=1,2,3
     * <p>
     * 查询所有角色的名称，并返回角色列表
     */
    List<Example> getListByIds();

    /**
     * 用户为多角色，使用,分割
     * 如role_ids=1,2,3
     * <p>
     * 查询所有角色的名称，并返回字符串
     */
    Example getNameByIds();
}

package com.choimroc.demo.application.example.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.choimroc.demo.application.example.entity.Example;
import com.choimroc.demo.application.example.mapper.ExampleMapper;
import com.choimroc.demo.application.example.service.ExampleService;
import com.choimroc.demo.tool.PageUtil;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author choimroc
 * @since 2019/10/17
 */
@Service
public class ExampleServiceImpl extends ServiceImpl<ExampleMapper, Example> implements ExampleService {
    private final ExampleMapper exampleMapper;
    private final SqlSessionFactory sessionFactory;

    public ExampleServiceImpl(ExampleMapper exampleMapper, SqlSessionFactory sessionFactory) {
        this.exampleMapper = exampleMapper;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public IPage<Example> getByPage(Long pageNumber, Long pageSize, String startDate, String endDate) {
        //普通分页
//        List<List<?>> objects = exampleMapper.selectByPage(pageSize, (pageNumber - 1) * pageSize, startDate, endDate);
        //条件优化分页
        List<List<?>> objects = exampleMapper.optimizationPage(pageSize, (pageNumber - 1) * pageSize, startDate, endDate);
        return PageUtil.handle(pageNumber, pageSize, objects);
    }

    @Override
    public boolean updateBatch(List<Example> examples) {
        if (examples == null) {
            return false;
        }
        //方法1 mybatis-plus3.3.0后的实现 待验证
        try (SqlSession batchSqlSession = SqlHelper.sqlSessionBatch(Example.class)) {
            ExampleMapper mapper = batchSqlSession.getMapper(ExampleMapper.class);
            for (Example item : examples) {
                mapper.updateExample(item);
            }
            batchSqlSession.flushStatements();
        }

        //方法2 依赖mybatis-plus,已过时
        executeBatch(sqlSession -> {
            ExampleMapper mapper = sqlSession.getMapper(ExampleMapper.class);
            for (Example item : examples) {
                mapper.updateExample(item);
            }
            sqlSession.flushStatements();
        });

        //方法3 注入式sessionFactory，待验证
        try (SqlSession session = sessionFactory.openSession(ExecutorType.BATCH)) {
            ExampleMapper mapper = session.getMapper(ExampleMapper.class);
            for (Example item : examples) {
                mapper.updateExample(item);
            }
            session.commit();
        }

        return true;
    }

    @Override
    public boolean saveReturnKey(Example example) {
        return exampleMapper.insertReturnKey(example) > 0;
    }
}

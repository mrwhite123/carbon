package com.choimroc.demo.application.base;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * 分页查询
 *
 * @author choimroc
 * @since 2020/4/20
 */
@Setter
@Getter
public class BasePageQuery {
    @NotNull(message = "{parameter.notNull.pageNumber}")
    @Min(value = 1, message = "{parameter.size.pageNumber}")
    private Long pageNumber;
    @NotNull(message = "{parameter.notNull.pageSize}")
    @Min(value = 5, message = "{parameter.size.pageSize}")
    @Max(value = 100, message = "{parameter.size.pageSize}")
    private Long pageSize;
}

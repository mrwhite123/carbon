package com.choimroc.demo.common.validator;

import com.choimroc.demo.common.validator.constraints.DateTime;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author choimroc
 * @since 2020/12/26
 */
public class DateTimeValidator implements ConstraintValidator<DateTime, String> {
    private String pattern;

    @Override
    public void initialize(DateTime parameters) {
        pattern = parameters.pattern();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || value.isEmpty()) {
            return true;
        }
        try {
            LocalDateTime.parse(value, DateTimeFormatter.ofPattern(pattern));
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}

package com.choimroc.demo.common;

/**
 * 常量
 *
 * @author choimroc
 * @since 2019/6/17
 */
public interface Constants {

    interface TrueOrFalse {
        Integer TRUE = 1;
        Integer FALSE = 0;
    }

    interface Prefix {

    }

    /**
     * redis分组前缀
     */
    interface RedisPrefix {
        //用户token
        String TOKEN = "user_token:";
    }
}

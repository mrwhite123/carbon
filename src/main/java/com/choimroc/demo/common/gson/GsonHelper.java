package com.choimroc.demo.common.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.InstanceCreator;
import com.google.gson.internal.ConstructorConstructor;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Gson 工具
 *
 * @author choimroc
 * @since 2019/3/8
 */
public enum GsonHelper {
    /**
     * 单例
     */
    INSTANCE;
    private final Gson gson;

    GsonHelper() {
        gson = create();
    }


    public Gson getGson() {
        return gson;
    }

    public String toJson(Object src) {
        return gson.toJson(src);
    }

    public <T> T json2Obj(String json, Class<T> classOfT) {
        return gson.fromJson(json, classOfT);
    }

    public <T> List<T> json2List(String json, Class<T> classOfT) {
        Type typeOfT = TypeToken.getParameterized(List.class, classOfT).getType();
        return gson.fromJson(json, typeOfT);
    }

    public <K, V> Map<K, V> json2Map(String json, Class<K> kClassOfT, Class<V> vClassOfT) {
        Type typeOfT = TypeToken.getParameterized(Map.class, kClassOfT, vClassOfT).getType();
        return gson.fromJson(json, typeOfT);
    }

    private Gson create() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls()
                .registerTypeAdapterFactory(TypeAdapters.newFactory(String.class, new StringAdapter()))
                .registerTypeAdapterFactory(createCollectionTypeAdapterFactory(gsonBuilder, false));
        return gsonBuilder.create();
    }

    /**
     * 如果有使用Gson的InstanceCreator,如
     * registerTypeAdapter(Bean.class,new InstanceCreator<Base>(){
     * public Bean createInstance(Type type){
     * return new Bean();}}
     * <p>
     * 则需要通过反射获取instanceCreators，否则直接使用new HashMap<>()即可
     */
    public CollectionTypeAdapterFactory createCollectionTypeAdapterFactory(GsonBuilder gsonBuilder, boolean instanceCreators) {
        if (instanceCreators) {
            try {
                Class<?> builder = gsonBuilder.getClass();
                Field f = builder.getDeclaredField("instanceCreators");
                f.setAccessible(true);
                @SuppressWarnings("unchecked")
                Map<Type, InstanceCreator<?>> val = (Map<Type, InstanceCreator<?>>) f.get(gsonBuilder);
                return new CollectionTypeAdapterFactory(new ConstructorConstructor(val));
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
                return new CollectionTypeAdapterFactory(new ConstructorConstructor(new HashMap<>()));
            }
        } else {
            return new CollectionTypeAdapterFactory(new ConstructorConstructor(new HashMap<>()));

        }
    }
}

## 一、使用说明
1、新项目需要修改的地方

- 第三级包名`demo`可以改为自己项目对应的包名

- `Application.java`：`@MapperScan` 中的路径改为自己项目中对应的包名

- 在`settings.gradle`中可以修改gradle项目名`rootProject.name`

2、配置`gradle`打包为war(如有需要才进行以下配置)

- 在`Application.java`同级目录下新增一个继承自`SpringBootServletInitializer`的类，如

```java
public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

}
```

- 在`build.gradle`的`plugins`节点加入` id 'war'`,如下

```groovy
plugins {
    id 'org.springframework.boot' version '2.2.0.RELEASE'
    id 'io.spring.dependency-management' version '1.0.8.RELEASE'
    id 'java'
    id 'war'
}
```

- 在`dependencies`节点中加入依赖，如下
```groovy
dependencies {
     providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
}
```
> PS:使用war包部署时，建议将`version`置空

3、使用开发环境配置文件
建议新建一个配置文件，如`application-local.yml`,并且该文件不使用git管理，方便多人协作开发;
然后在启动服务时使用program arguments : `--spring.profiles.active=local` 指定开发环境。

## 二、目录结构

### 1、包结构 

|  路径   | 说明  |
|  ----  | ----  |
| `com/choimroc/demo/application`  | 业务模块 |
| `com/choimroc/demo/common/base`  | 基类 |
| `com/choimroc/demo/common/config`  | spring及相关类库的配置 |
| `com/choimroc/demo/common/convert`  | request参数转换相关 |
| `com/choimroc/demo/common/exception`  | 全局异常处理 |
| `com/choimroc/demo/common/gson`  | gson相关 |
| `com/choimroc/demo/common/locale`  | 国际化相关 |
| `com/choimroc/demo/common/result`  | response相关 |
| `com/choimroc/demo/common/validator`  | request参数校验相关 |
| `com/choimroc/demo/security/auth`  | 用户权限校验相关 |
| `com/choimroc/demo/security/lock`  | 缓存锁(防止重复提交等)相关 |
| `com/choimroc/demo/tool`  | 工具类 |

### 2.资源结构

|  路径   | 说明  |
|  ----  | ----  |
| `resources/i18n`  | 多语言文本资源，搭配国际化使用 |
| `resources/templates`  | 自动代码生成相关模板文件 |
| `com/choimroc/demo/common/config`  | spring及相关类库的配置 |
| `resources/application.yml`  | spring项目配置文件 |
| `resources/logback-spring.xml`  | 项目日志配置文件 |


## 三、详细说明

1、业务模块
- `com/choimroc/demo/application`目录下每个业务模块使用单独的目录，例如`com/choimroc/demo/application/example`;
- 每个业务模块下包含下列几个目录(仅供参考)
  
  |  目录名   | 说明  |
  |  ----  | ----  |
  | `controller`  | 接口控制器 |
  | `entity`  | 数据库实体类 |
  | `mapper`  | MyBatis数据库操作 |
  | `service`  | 业务逻辑 |
  | `body`  | RequestBody和ResponseBody |

2、权限验证
- 验证文件为`com/choimroc/demo/security/auth/SecurityInterceptor.java`,需要自行修改为自己项目的验证逻辑
- 使用`@IgnoreSecurity`注解过滤掉不需要进行权限验证的接口


